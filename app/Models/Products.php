<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Products extends Model
{
    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    /**
     * Get Products function
     * @param string $column
     * @param string $value
     * @return array
     * @author NguyenNguyen
     */
    public function getProducts($column, $value)
    {
        return DB::table('products')->where($column, $value)->get()->toArray();
    }

    /**
     * Insert Products function
     * @param array $data
     * @return void
     * @author NguyenNguyen
     */
    public function insertProducts($data)
    {
        DB::table('products')->insert($data);
    }

    /**
     * Update Products function
     * @param string $column
     * @param data $data
     * @return void
     * @author NguyenNguyen
     */
    public function updateProducts($column, $data)
    {
        DB::table('products')
            ->where($column, $data[$column])
            ->update($data);
    }
}
