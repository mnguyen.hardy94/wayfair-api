<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Inventory extends Model
{
    /**
     * Get Inventory function
     * @param string $column
     * @param string $value
     * @return array
     * @author NguyenNguyen
     */
    public function getInventory($column, $value)
    {
        return DB::table('inventory')->where($column, $value)->get()->toArray();
    }

    /**
     * Insert Inventory function
     * @param array $data
     * @return void
     * @author NguyenNguyen
     */
    public function insertInventory($data)
    {
        DB::table('inventory')->insert($data);
    }

    /**
     * Update Inventory function
     * @param string $column
     * @param data $data
     * @return void
     * @author NguyenNguyen
     */
    public function updateInventory($column, $data)
    {
        DB::table('inventory')
            ->where($column, $data[$column])
            ->update($data);
    }
}
