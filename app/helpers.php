<?php

use Illuminate\Support\Facades\Storage;

if (!function_exists('getCredential')) {
    /**
     * Get Credential function
     * @return array
     * @author NguyenNguyen
     */
    function getCredential()
    {
        $path = '/credential/.inf.cre';

        if (!Storage::exists($path))
            exit(__('IAM file not exit!'));

        if (!Storage::get($path))
            exit(__('IAM file cannot empty!'));

        $credential = json_decode(Storage::get($path), true);

        if (!isset($credential['token']) || !$credential['token'])
            exit(__('Missing parameter: token'));

        if (!isset($credential['expire_time']) || !$credential['expire_time'])
            exit(__('Missing parameter: expire_time'));

        return $credential;
    }
}


