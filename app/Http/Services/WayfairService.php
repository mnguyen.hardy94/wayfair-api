<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Storage;
use hmdjsc\WayfairAPI\Client;

/**
 * Class WayfairService
 * @package App\Http\Services
 */
class WayfairService
{

    /**
     * Get Credential function
     * @return array
     * @author NguyenNguyen
     */
    public function getCredential()
    {
        $path = 'credential/inf.cre';
        $credential = json_decode(Storage::get($path), true);
        return $credential;
    }

    /**
     * Get Token function
     * @return string
     * @author NguyenNguyen
     */
    public function getToken()
    {
        $path = 'credential/inf_oauth.cre';

        if (!Storage::exists($path))
            exit(__('File Inf Oauth not exit!'));

        if (!Storage::get($path))
            exit(__('File Inf Oauth cannot empty!'));

        $credentialDetails = json_decode(Storage::get($path), true);

        if($credentialDetails['expire_time'] >= date("Y-m-d H:i:s")){
            if (is_array($credentialDetails) && array_key_exists('token', $credentialDetails))
                return $credentialDetails['token'];
        }else{ //Access token Expired, refresh new token
            $credential = $this->getCredential();

            $client = new Client();
            $token = $client->fetchToken($credential['client_id'], $credential['client_secret']);
            $this->saveToken($token);
            return $token;
        }

        return $credentialDetails['token'];
    }

    /**
     * Save Token function
     * @param string $token
     * @return void
     * @author NguyenNguyen
     */
    public function saveToken($token)
    {
        $path = 'credential/inf_oauth.cre';
        Storage::disk('local')->put($path, json_encode(array(
            'token' => $token,
            'expire_time' => date("Y-m-d H:i:s", time() + 86400)
        )));
    }

}