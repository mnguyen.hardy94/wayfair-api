<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Http\Services\WayfairService;
use App\Models\Inventory;
use App\Models\Products;
use hmdjsc\WayfairAPI\Client;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    private $wayfairService;

    /**
     * ProductController constructor.
     * @param WayfairService $wayfairService
     * @ignore
     */
    public function __construct(WayfairService $wayfairService)
    {
        $this->wayfairService   = $wayfairService;
        $this->token            = $wayfairService->getToken();
    }

    /**
     * Get Cata Logs function
     * @return void
     * @author NguyenNguyen
     */
    public function getCataLogs()
    {
        $client = new Client();
        $dataAPI = $client->GetProductCatalogs($this->token, array('limit' => 50));

        foreach ($dataAPI['data']['productCatalogs'] as $item)
        {
            $objProduct = new Products();
            $checkItem = $objProduct->getProducts('supplier_part_number', $item['supplierPartNumber']);

            $itemProcess = [
                'supplier_part_number'      => $item['supplierPartNumber'],
                'manufacture_model_number'  => $item['manufacturerModelNumber'],
                'manufacture_name'          => $item['manufacturerName'],
                'upc'                       => $item['upc'],
                'supplier_id'               => $item['supplierId'],
                'product_name'              => $item['productName'],
                'collection_name'           => $item['collectionName'],
                'wholesale_price'           => $item['wholesalePrice'],
                'map_price'                 => $item['mapPrice'],
                'full_retail_price'         => $item['fullRetailPrice'],
                'mininum_order_quantity'    => $item['minimumOrderQuantity'],
                'force_multiples'           => $item['forceMultiples'],
                'display_set_quantity'      => $item['displaySetQuantity'],
                'manufacturer_country'      => $item['manufacturerCountry'],
                'harmonized_code'           => $item['harmonizedCode'],
                'canada_code'               => $item['canadaCode'],
                'lead_time'                 => $item['leadTime'],
                'lead_time_for_replacement_parts'  => $item['leadTimeForReplacementParts'],
                'sku'                       => $item['sku'],
                'sku_status'                => $item['skuStatus'],
                'sku_sub_status'            => $item['skuSubstatus'],
                'white_labeled'             => $item['whiteLabeled'],
                'wayfair_class'             => $item['wayfairClass'],
                'update_time'               => date('Y-m-d H:i:s'),
            ];

            if(count($checkItem))
                $objProduct->updateProducts('supplier_part_number', $itemProcess);
            else
                $objProduct->insertProducts($itemProcess);
        }

        echo '<pre>';
        print_r($dataAPI);
        echo '</pre>';
    }

    /**
     * Get Inventory function
     * @return void
     * @author NguyenNguyen
     */
    public function getInventory()
    {
        $client = new Client();
        //array('limit' => 100,'offset' => 2)
        $dataAPI = $client->GetProductInventory($this->token, array('limit' => 100));

        $objProduct = new Products();
        $objInventory = new Inventory();

        foreach ($dataAPI['data']['inventory'] as $item)
        {
            $itemProcess = [
                'product_id'            => 0,
                'supplier_part_number'  => $item['supplierPartNumber'],
                'quantity_on_hand'      => $item['quantityOnHand'],
                'quantity_backordered'  => $item['quantityBackordered'],
                'quantity_on_order'     => $item['quantityOnOrder'],
                'discontinued'          => $item['discontinued'],
                'item_next_availability_date'=> $item['itemNextAvailabilityDate'],
                'update_time'           => date('Y-m-d H:i:s'),
            ];
            $productInfo = $objProduct->getProducts('supplier_part_number', $item['supplierPartNumber']);

            if($productInfo)
                $itemProcess['product_id'] = $productInfo[0]->id;

            $checkItem = $objInventory->getInventory('supplier_part_number', $item['supplierPartNumber']);

            if(count($checkItem))
                $objInventory->updateInventory('supplier_part_number', $itemProcess);
            else
                $objInventory->insertInventory($itemProcess);
        }

        echo '<pre>';
        print_r($dataAPI);
        echo '</pre>';
    }

    /**
     * Get Dropship Purchase Orders function
     * @return void
     * @author NguyenNguyen
     */
    public function getDropshipPurchaseOrders()
    {
        $client = new Client();
        $dataAPI = $client->GetDropshipPurchaseOrders($this->token, array('limit' => 2));

        echo '<pre>';
        print_r($dataAPI);
        echo '</pre>';
    }
}
