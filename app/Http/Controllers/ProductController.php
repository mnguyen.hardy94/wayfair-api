<?php

namespace App\Http\Controllers;
use App\Http\Services\WayfairService;
use App\Models\Products;
use hmdjsc\WayfairAPI\Client;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{
    private $wayfairService;

    /**
     * ProductController constructor.
     * @param WayfairService $wayfairService
     * @ignore
     */
    public function __construct(WayfairService $wayfairService)
    {
        $this->wayfairService   = $wayfairService;
        $this->token            = $wayfairService->getToken();
    }

    /**
     * Show function
     * @return void
     * @author NguyenNguyen
     */
    public function getProductCataLogs()
    {
        $client = new Client();
        $dataAPI = $client->GetProductCatalogs($this->token, array('limit' => 50));

        foreach ($dataAPI['data']['productCatalogs'] as $item)
        {
            $objProduct = new Products();
            $checkItem = $objProduct->getProducts('model_number', $item['manufacturerModelNumber']);

            $itemProcess = [
                'product_id'        => 0,
                'model_number'      => $item['manufacturerModelNumber'],
                'wholesale_price'   => $item['wholesalePrice'],
                'lead_time'         => $item['leadTime'],
                'lead_time_for_replacement_parts' => $item['leadTimeForReplacementParts'],
                'status'            => $item['skuStatus'],
                'sub_status'        => $item['skuSubstatus'],
                'update_time'       => date('Y-m-d H:i:s'),
            ];

            if(count($checkItem))
                $objProduct->updateProducts('model_number', $itemProcess);
            else
                $objProduct->insertProducts($itemProcess);
        }
    }

    /**
     * Get Product Inventory function
     * @return void
     * @author NguyenNguyen
     */
    public function getProductInventory()
    {
        $client = new Client();
        $dataAPI = $client->GetProductInventory($this->token, array('limit' => 2,'offset' => 4));

        echo '<pre>';
        print_r($dataAPI);
        echo '</pre>';
    }

    /**
     * Get Dropship Purchase Orders function
     * @return void
     * @author NguyenNguyen
     */
    public function getDropshipPurchaseOrders()
    {
        $client = new Client();
        $dataAPI = $client->GetDropshipPurchaseOrders($this->token, array('limit' => 2));

        echo '<pre>';
        print_r($dataAPI);
        echo '</pre>';
    }

}