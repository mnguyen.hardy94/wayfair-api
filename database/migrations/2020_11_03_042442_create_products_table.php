<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('supplier_part_number');
            $table->string('manufacture_model_number');
            $table->string('manufacture_name');
            $table->string('upc');
            $table->string('supplier_id');
            $table->string('product_name');
            $table->string('collection_name')->nullable();
            $table->float('wholesale_price');
            $table->float('map_price')->nullable();
            $table->float('full_retail_price')->nullable();
            $table->integer('mininum_order_quantity');
            $table->integer('force_multiples');
            $table->integer('display_set_quantity');
            $table->string('manufacturer_country')->nullable();
            $table->string('harmonized_code')->nullable();
            $table->string('canada_code')->nullable();
            $table->integer('lead_time');
            $table->integer('lead_time_for_replacement_parts');
            $table->string('sku');
            $table->integer('quantity')->default(0);
            $table->string('sku_status')->nullable();
            $table->string('sku_sub_status')->nullable();
            $table->string('white_labeled');
            $table->string('wayfair_class');
            $table->dateTime('update_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
