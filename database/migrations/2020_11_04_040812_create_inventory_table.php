<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id');
            $table->string('warehouse_id')->nullable();
            $table->string('supplier_part_number');
            $table->integer('quantity_on_hand')->default(0);
            $table->integer('quantity_backordered')->default(0);
            $table->integer('quantity_on_order')->default(0);
            $table->integer('discontinued')->default(0);
            $table->dateTime('item_next_availability_date')->nullable();
            $table->dateTime('update_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory');
    }
}
